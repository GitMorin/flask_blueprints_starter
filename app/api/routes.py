from flask import Blueprint

mod = Blueprint('api', __name__) # __name__ is the current module name

@mod.route('/getstuff')
def getStuff():
  return '{"result": "You are in the API!!"}'