from flask import Blueprint

mod = Blueprint('site', __name__) # __name__ is the current module name

@mod.route('/')
@mod.route('/homepage')
def homepage():
  return '<h1>You are on the home page!!</h1>'